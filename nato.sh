#!/bin/bash
#
# Command line script to install my system's shit.
#
# Exit values:
#  0 on success
#  1 on failure
#

# Name of the script
SCRIPT=$( basename "$0" )

# Current version
VERSION="0.0.1"

# Some color
ORANGE='\033[0;33m'
NOCOLOR='\033[0m'

#
# Message to display for usage and help.
#
function usage
{
    local txt=(
        ""
        "Usage: $SCRIPT [options] <command> [arguments]"
        ""
        "Command:"
        "  documents          Install Documents."
        "  build              Build arm-console project."
        "  flash [port]       Build & Flash project on [port]."
        "  clang              Runs clang-format on src/ directory."
        ""
        "Options:"
        "  --help, -h     Print help."
        "  --version, -v  Print version."
    )
    
    printf "%s\n" "${txt[@]}"
}

#
# Message to display when bad usage.
#
function badUsage
{
    local message="$1"
    local txt=(
        "For an overview of the command, execute: $SCRIPT -h"
    )
    
    [[ $message ]] && printf "$message\n"
    
    printf "%s\n" "${txt[@]}"
}

#
# Message to display for version.
#
function version
{
    ecco "version ${VERSION}"
}

#
# Customized echo
#
function ecco
{
    printf "${ORANGE}[nato]${NOCOLOR} $1 \n"
}

#
# Setup Keychain. Add keys from .ssh/ and setup git with SSH
# 
# https://www.cyberciti.biz/faq/ssh-passwordless-login-with-keychain-for-scripts/
# Another link with good explanation around all ssh-config and infra
# https://goteleport.com/blog/ssh-config/
# 
# For the future change keys to a newer more safe like ed25519
# read about here https://goteleport.com/blog/comparing-ssh-keys/
#
# Now trying this technique
# https://fabianlee.org/2021/04/05/ubuntu-loading-a-key-into-ssh-agent-at-login-with-a-user-level-systemd-service/
# WARN Have to change fabianlee service because he made for adding only one key
# we manage multiple for now one ssh hetzner servers and one for github
#
# FUCKK!!!! nothing did workout BUT SHIT no one talks about the fucking ssh-config option
# AddKeysToAgent so we only need to have this option on ssh-config and we are good to go
# let's how it goes with github stuff
#
# On regards to github I'am following the tutorial below to have a private email
# kept in privacy when commiting to micro$hit github
# https://techyarsal.medium.com/how-to-setup-git-the-proper-way-part-3-setting-up-ssh-agent-and-public-email-b2b3c7a4e5ff
#
# DONE! ahahha just this, thanks to the internet and sometimes we forgetting to
# search in the last year only we can lost a day or two just to setup keys
# anyways, still need to update my keys to a "more secure" version
# BTW thanks to pa4080 at https://askubuntu.com/a/1097078 for the tip on setup
# ssh config file 
#
# Closing up here just copy current ssh/ folder
function app-git
{
  if [ ! -d "~/.ssh/" ]; then
    ecco "Could not find ssh/ please copy the folder in place we need for git too."
    exit 1
  fi

  ecco "Setup git... AAAHHH check in the future if we need to apt install git in a fresh install"
  
  git config --global user.name "aviler"
  git config --global user.email "399399+aviler@users.noreply.github.com"
  
  # reusing function to do more things
  # install nvim and add it as git main text editor
  sudo apt update
  sudo apt install nvim
  git config --global core.editor "nvim"
  
  ecco "Done! You can start cloning your shit now." 
}

#
# More like a reminder on steps to setup the system
# This should only ecco something
function app-firefox
{
  ecco "Just open Firefox and sign-in yourself"
  ecco "hopefully everything will be there..."
  ecco "ah you still need to login in some plugins"
  ecco "and give a nice look into the settings, just in case..."
  
  
  ecco "Done!" 
}

#
# Pop_OS don't ask for hostname on installation and this is very important hahaha
# 
function app-hostname
{
  if [ $# -eq 0 ]; then
    ecco "No hostname argument supplied."
    exit 1
  fi

  ecco "Setup new hostname $1"
  
  sudo hostnamectl set-hostname $1
  sudo sed -i 's/pop-os/'$1'/g' /etc/hosts
  
  ecco "Done!" 
}

#
# Installs M$ Teams
# 
function app-teams
{
#  if [ $# -eq 0 ]; then
#    ecco "No hostname argument supplied."
#    exit 1
#  fi

#  ecco "Setup new hostname $1"
  
  ecco "Installing microsoft Teams"

  # Do we even have flatpak installed out-of-the-box?  
  flatpak update
  # flatpak search microsoft teams // WAT?! removing this
  
  
  # In theory we could just open the app and change the theme to dark, please.
  # The fucker app has this auto-start application option automatically checked on
  # I will check if stills auto enables itself like older versions did.
  # It does not autostart \o/ just need to config and will only autostart if 
  # we say so through pop_os, i guess. TODO check how to make autostart lols
  
  # Below I am again reusing this function to install other things that I found
  # out during this session
  flatpak install flathub org.flameshot.Flameshot
  flatpak install flathub net.cozic.joplin_desktop
  sudo apt update
  sudo apt install fonts-hack-ttf # didnt try this but is possible 
  
  ecco "Done!" 
}



#
# Function for install Documents
#
function app-documents
{
  ecco "Installing Documents..."
    
  #touch ~/Documents/folder_installed.txt
  #Instalar algum self hosted dropbox e por pra rodar ai

  #sudo add-apt-repository ppa:nextcloud-devs/client
  #sudo apt update
  #sudo apt install nextcloud-client -y

  ecco "Done!"
}

#
# Function to building the arm-console project
#
function app-build
{
    ecco "Building arm-console..."
    
    docker run --rm -v ${PWD}:/workdir/qwic-embedded/arm-console qwic-embedded /bin/bash -c 'cd /workdir/qwic-embedded && west build -p always -b nrf5340pdk_nrf5340_cpuapp arm-console/ && cp -r build/ /workdir/qwic-embedded/arm-console/build'
    
    ecco "Done!"
}

#
# Function to build arm-console project & flash it on the given port
#
function app-flash
{
    if [ $# -eq 0 ]; then
        ecco "No port argument supplied."
    fi
    
    docker run --rm -v ${PWD}:/workdir/qwic-embedded/arm-console --device=$1 --privileged qwic-embedded /bin/bash -c 'cd /workdir/qwic-embedded && west build -p always -b nrf5340pdk_nrf5340_cpuapp arm-console/ && west flash'
    
    echo
    ecco "Flashing complete!"
}

#
# Function to run clang-format on src folder and format the code
#
function app-clang
{
    docker run --name arm-console -d qwic-embedded tail -f /dev/null
    
    find ./src -type f -iname \*.h -o -iname \*.c | xargs -I@ /bin/bash -c "\
        tmpfile=\$(mktemp /tmp/clang-formatted.XXXXXX) && \
        docker exec -i arm-console clang-format < @ > \$tmpfile && \
    cmp --silent @ \$tmpfile || (mv \$tmpfile @ && echo @ formatted.)"
    
    docker kill arm-console
    
    docker rm arm-console
    
    echo
    ecco "Clang formatting complete!"
}

#
# Process options/commands
#

# docker is required
# if [ ! -x "$(command -v docker)" ]; then
#     ecco "Docker not found! Check README.rst on how to install it."
#     exit 1
# fi

# being in the arm-console path with a Dockerfile alogside is required
# if [ ! -f "Dockerfile" ]; then
#     ecco "Dockerfile not found! Should be right here. Start README.rst docker steps again."
#     exit 1
# fi


while (( $# ))
do
    case "$1" in
        
        --help | -h)
            usage
            exit 0
        ;;
        
        --version | -v)
            version
            exit 0
        ;;
        
        hostname     \
        | documents  \
        | build      \
        | flash      \
        | clang)
            command=$1
            shift
            app-$command $*
            exit 0
        ;;
        
        *)
            badUsage "Option/Command not recognized."
            exit 1
        ;;
        
    esac
done

badUsage
exit 1
